import UIKit
//optional bilding
var str = "Hello, playground"
class Person {
    var ten: String
    var bietdanh: String?
    var money: String
    init(ten: String, bietdanh: String?) {
        self.ten = ten
        self.bietdanh = bietdanh
    }
    
}
func goiTen(person: Person)
{
    if let goiten = person.bietdanh{
        print("co the co hoac khong\(goiten)")
    } else {
        print("goi ten that\(person.ten)")
    }
}
goiTen(person: Person(ten: "Chinh", bietdanh: "cute"))
goiTen(person: Person(ten: "Chinh", bietdanh: nil))


func diBAr(person: Person) {
    guard let money = person.money else {
        return
    }
    print("het tien   \(money) ")
}



//cham than la non unwapoptional tu dong unwap khi duoc goi den vi vay can kiem tra )
//khac phuc bang cach su dung optional bilding hoac force unwap
//auto unwap optional(implicit unwq), chac chan no la gia tri khong nil
var string: String! = nil
var otherString: String = "abc"
print(string)
print(otherString)
string = otherString



//can phai forc unwap
var name: String? = nil
var otherName: String = "abc"
print(name!)
print(otherName)
name = otherName


var nonOptioalString: String
nonOptioalString = string


//optional bilding va implicit unwap optioal deu la optional nghia la deu co the nhan gia tri nil
//implicit umwap khac optional la no tu dong force unwap khi duuoc goi den nen vi vay khi duoc su dung inplicit va optional deu phai kiem tra truoc khi su dung



var label: UILabel? = UILabel()
//optional chainning (qua trinh truy cap vao mot property moot method neu co gia tri thi gan khong co gia tri thi khong gan nua)
label?.text

